function file_size(file)
    local current_pos = file:seek()
    local size = file:seek("end")
    file:seek("set", current)
    return size
end

filename = arg[1] or "../testcases.txt"
local file = assert(io.open(filename, "rb"))
print(file_size(file))