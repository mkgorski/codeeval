#!/bin/bash

LEVEL=$1
CHALLENGE_NAME=$2
LANG=$3

if [ "$LEVEL" = "" ] || [ "$CHALLENGE_NAME" = "" ] || [ "$LANG" = "" ]; then
    echo "Not all parameters are present. Usage:"
    echo "$0 level name language"
    echo "       level    - easy | moderate | hard"
    echo "       name     - name of the challenge"
    echo "       language - c | cpp | py2 | lua ..."
    exit 1
fi

mkdir -p "$LEVEL"/"$CHALLENGE_NAME"/"$LANG"
touch "$LEVEL"/"$CHALLENGE_NAME"/"$LANG"/"$CHALLENGE_NAME"."$LANG"
touch "$LEVEL"/"$CHALLENGE_NAME"/testcases.txt
git add "$LEVEL"/"$CHALLENGE_NAME"
git commit -m "New empty challenge: $CHALLENGE_NAME"
